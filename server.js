// load the things we need
var express = require('express');
var app = express();
var port = 8080;

// set the view engine to ejs
app.set('view engine', 'ejs');

// using app.use to serve up static CSS files in public/assets/ folder when /public link is called in ejs files
// app.use("/route", express.static("foldername"));
app.use(express.static('public'));

// use res.render to load up an ejs view file

// connexion page 
app.get('/', function (req, res) {
    res.render('pages/connexion');
});

// inscription page 
app.get('/inscription', function (req, res) {
    res.render('pages/inscription');
});

// inscription page proprio
app.get('/inscription_proprio', function (req, res) {
    res.render('pages/inscription_proprio');
});

// inscription page locataire
app.get('/inscription_locataire', function (req, res) {
    res.render('pages/inscription_locataire');
});

// mon compte
app.get('/compte', function (req, res) {
    res.render('pages/compte');
});

// foyer_locataire page 
app.get('/foyer_locataire', function (req, res) {
    res.render('pages/foyer_locataire');
});

// foyer_proprio page 
app.get('/foyer_proprio', function (req, res) {
    res.render('pages/foyer_proprio');
});

// conso page 
app.get('/conso', function (req, res) {
    res.render('pages/conso');
});

// doc page 
app.get('/doc', function (req, res) {
    res.render('pages/doc');
});

// about page 
app.get('/about', function (req, res) {
    res.render('pages/about');
});





//-_-_-_-_-_-_-_-_-_-_-_-_-
// Ecoute
app.listen(port);
console.log(port + ' est le port magique pour baisser sa consommation');

